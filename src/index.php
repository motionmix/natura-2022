<?php
	// Global Vars
	include_once './app/vars.php';

	// Local vars
	$is_home = true;
	$page_title = 'Santa Monica Nail Salon';
	$page_desc = 'Natura Nail Spa is a full service nail salon in Santa Monica, California. We specialize in manicures, pedicures, and other nail services.';
	$page_slug = '';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './app/before-content.php';

	// Inner HTML of <main>.
	// Includes: document meta, heading, content, footing, aside.
	include_once 'content.php';

	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './app/after-content.php';
?>