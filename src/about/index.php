<?php
	// Global Vars
	include_once './../app/vars.php';

	// Local vars
	$page_desc = 'All about Natura Nail Spa, your favorite neighborhood Santa Monica nail salon!';
	$page_slug = 'about';
	$page_title = 'About';
	
	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../app/before-content.php';

	// Inner HTML of <main>.
	// Includes: document meta, heading, content, footing, aside.
	include_once 'content.php';
	include_once './../app/content-services.php';

	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../app/after-content.php';
?>