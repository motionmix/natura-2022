<div class="container-fluid px-0">

  <!-- HERO -->
  
  <div class="px-4 py-5 mb-5 text-center bg-hero ar-4x1">
    <div class="rounded-3 py-5 px-3 d-flex flex-column mx-auto container lead bg-white-_875">
			<h1 class="fw-normal mb-0">
        <small>All About<br></small>
        <span class="cinzel display-5 fw-bold">Natura Nail Spa</span>
      </h1>

			<div class="col-lg-8 mx-auto">
				<p class="lead">Brought to you by Interior Designer, <b>Tomas Calderon</b> &mdash; the former 922 Nail Spot and has been completely transformed into lavish, relaxing <b>Natura Nail Spa</b>!</p>
			</div>

			<?php include './../app/actions-strip.php'; ?>

    </div>
  </div>
</div>
