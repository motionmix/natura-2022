<!-- SEO Meta -->

<title><?php  
  if ($is_home == true)
  {
    echo $site_name . ' &mdash; ' . $page_title; 
  } 
  else
  {
    echo $page_title . ' &mdash; ' . $site_name;
  } 
?></title>

<meta name="description" content="<?php echo $page_desc ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php 
  if (($is_home == true) && ($has_g_verification == true) )
  { ?><!-- Google Search Console verification. Only required on '/'. -->
<meta name="google-site-verification" content="<?php echo $g_verification ?>"><?php 
  }

  include_once 'seo-meta-opengraph.php';  // OpenGraph
  include_once 'seo-script-meta.php';     // JSON+LD structured data
?>

<link rel="canonical" href="<?php echo $site_url ?>/<?php 
  echo $page_slug; if (!($page_slug == '')) 
  { 
    echo '/'; 
  } 
?>">

<?php // [/seo-meta] ?>