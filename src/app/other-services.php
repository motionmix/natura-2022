<?php // [other-services] ?>
<section class="container other-services text-center <?php echo $page_slug ?>">
  <h3 class="cinzel my-5">Other Services</h3>

  <div class="d-lg-flex flex-lg-equal my-md-3 row px-0">
    <aside class="hands-block bg-dark pt-3 px-3 pt-md-5 px-md-5 text-white overflow-hidden col-12 col-12 col-sm-12 col-lg-4 relative">
      <div class="my-3 py-3">
        <span class="leading">FOR</span>
        <h4 class="display-6 cinzel mt-0">Hands</h4>
      </div>
      <a href="/services/hands/" class="d-block bg-hands shadow mx-auto formfactor semi-opaque service-block-cta"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>

    <aside class="feet-block bg-light pt-3 px-3 pt-md-5 px-md-5 overflow-hidden col-12 col-sm-12 col-lg-4 relative">
      <div class="my-3 p-3">
        <span class="leading">FOR</span>
        <h4 class="display-6 cinzel">Feet</h4>
      </div>
      <a href="/services/feet/" class="d-block bg-feet formfactor shadow mx-auto semi-opaque service-block-cta"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>

    <aside class="wax-block bg-primary pt-3 px-3 pt-md-5 px-md-5 text-white overflow-hidden col-12 col-sm-12 col-lg-4 relative">
      <div class="my-3 py-3">
        <span class="leading">FOR</span>
        <h4 class="display-6 cinzel">Wax</h4>
      </div>
      <a href="/services/wax/" class="bg-wax formfactor shadow mx-auto d-block semi-opaque service-block-cta"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>

    <aside class="extras-block bg-dark pt-3 px-3 pt-md-5 px-md-5 text-white overflow-hidden col-12 col-sm-12 col-lg-4 relative">
      <div class="my-3 p-3">
        <span class="leading">FOR</span>
        <h4 class="display-6 cinzel">Add-ons</h4>
      </div>
      <a href="/services/extras/" class="d-block bg-extras formfactor shadow mx-auto semi-opaque service-block-cta"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>
  </div>
</section>
<?php // [/other-services] ?>