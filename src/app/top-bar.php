<?php // [top-bar] ?>
<!-- Top Bar -->

<nav class="navbar navbar-expand-lg navbar-dark bg-dark" aria-label="Offcanvas navbar large">
  <div class="container-fluid">

    <a class="navbar-brand d-inline-block py-0" href="/">
      <img class="mx-auto me-2" src="/assets/img/n-icon-88x88.png" alt="<?php echo $site_name ?> logo mark" width="40" height="40">
      <span class="lh-40"><?php echo $site_name ?></span>
    </a>
    
    <!-- OFF CANVAS NAV -->

    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar" title="navigation menu">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="offcanvas offcanvas-end text-white bg-dark" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
      <div class="offcanvas-header">
        <strong class="offcanvas-title cinzel" id="offcanvasNavbarLabel"><?php echo $site_name ?></strong>
        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      </div>
      <div class="offcanvas-body pt-0">

        <!-- OFF CANVAS NAV ITEMS -->

        <ul class="navbar-nav justify-content-end flex-grow-1">
          <li class="nav-item pe-3">
            <a href="/about/" class="nav-link">About</a>
          </li>
          <li class="nav-item pe-3">
            <a href="/gallery/" class="nav-link">Gallery</a>
          </li>
          <li class="nav-item pe-3">
            <a href="/services/" class="nav-link">Services</a>
          </li>
          <li class="nav-item pe-3">
            <a href="/contact/" class="nav-link">Contact</a>
          </li>

          <!-- CALL BUTTON -->

          <li class="nav-item px-1 my-3 my-lg-0">
            <a class="nav-link text-white btn btn-outline-secondary btn-md border-white" href="tel:<?php echo $phone_num ?>" rel="noopener" target="_blank"><svg width="16" height="16" class="icon-offset-t"><use xlink:href="#phone"/></svg>&nbsp; 310-395-5652</a>
          </li>
        </ul>

        <!-- OFF CANVAS SOCIAL -->

        <ul class="nav nav-pills d-lg-none d-flex justify-content-center offcanvas-social">
          <li class="nav-item px-1">
                
            <a class="nav-link text-white px-2" href="https://facebook.com/<?php echo $site_slug ?>" target="_blank" rel="noopener">
              <span class="visually-hidden">Facebook Business Page</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#facebook"></use></svg>
            </a>
          
          </li>
          <li class="nav-item px-1">
            <a class="nav-link text-white px-2" href="<?php echo $g_map_url ?>" target="_blank" rel="noopener">
              <span class="visually-hidden">Google Business Page</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#google"></use></svg>
            </a>
          </li>
          <li class="nav-item px-1">
            <a class="nav-link text-white px-2" href="https://instagram.com/<?php echo $ig_handle ?>" target="_blank" rel="noopener">
              <span class="visually-hidden">Instagram Profile</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#instagram"></use></svg>
            </a>
          </li>
          <li class="nav-item px-1">
            <a class="nav-link text-white px-2" href="<?php echo $yelp_url ?>" target="_blank" rel="noopener">
              <span class="visually-hidden">Yelp Listing</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#yelp"></use></svg>
            </a>
          </li>
          <li class="nav-item px-1">
            <a class="nav-link text-white px-2" href="https://twitter.com/<?php echo $site_slug ?>" target="_blank">
              <span class="visually-hidden">Twitter Feed</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#twitter"></use></svg>
            </a>
          </li>
        </ul>

      </div>
    </div>

  </div>
</nav>

<?php // [/top-bar] ?>