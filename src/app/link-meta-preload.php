<?php // [link-meta-preload] ?>

<!-- Preload Core CSS -->

<link rel="prefetch preload" as="style" href="https://fonts.googleapis.com/css2?family=Cinzel+Decorative:400,700&display=swap">    
<link rel="prefetch preload" as="style" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" crossorigin="anonymous" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor">
<?php 
  if ($is_gallery == true) 
  { ?>

<!-- Preload Gallery CSS -->

<link rel="prefetch preload" as="style" href="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css">
<link rel="prefetch preload" as="style" href="/assets/css/custom-mfp.css">
<?php 
  } ?>

<!-- Preload Core JS -->

<!-- <link rel="prefetch preload" as="script" href="https://www.googletagmanager.com/gtag/js?id=<?php echo $ga_id ?>"> -->
<!-- <link rel="prefetch preload" as="script" href="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk"> -->
<link rel="prefetch preload" as="script" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" crossorigin="anonymous" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy">

<?php 
  if ($is_gallery == true) 
  { ?>
<!-- Preload Gallery JS -->

<link rel="prefetch preload" as="script" href="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js">
<link rel="prefetch preload" as="script" href="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js">
<?php 
  } ?>

<?php // [/link-meta-preload] ?>