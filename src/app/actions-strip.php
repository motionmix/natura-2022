<!-- ACTIONS STRIP -->

<div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
  <!-- CALL -->
  <a href="tel:13103955652" target="_blank" rel="noopener" class="btn btn-primary btn-lg px-4 gap-3">
    <svg width="16" height="16" class="icon-offset-tl"><use xlink:href="#phone"></use></svg>&nbsp;
    310-395-5652
  </a>
  
  <!-- BOOK YELP -->
  <a href="https://www.yelp.com/biz/natura-nail-spa-santa-monica-2" target="_blank" rel="noopener" class="btn btn-secondary btn-lg px-4">
    <svg width="20" height="20" class="icon-offset-tl"><use xlink:href="#yelp"></use></svg>&nbsp;
    Book Online
  </a>
</div>

<?php // [/actions-strip] ?>