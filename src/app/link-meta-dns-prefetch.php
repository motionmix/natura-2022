<?php // [link-meta-dns-prefetch] ?>
<!-- Link Meta DNS Prefetch -->

<!-- Bootstrap -->
<link rel="dns-prefetch preconnect" href="https://cdn.jsdelivr.net" crossorigin>

<!-- Analytics -->
<link rel="dns-prefetch preconnect" href="https://www.googletagmanager.com" crossorigin>
<link rel="dns-prefetch preconnect" href="https://www.google-analytics.com" crossorigin>
<link rel="dns-prefetch preconnect" href="https://www.google.com" crossorigin>

<!-- Fonts -->
<link rel="dns-prefetch preconnect" href="https://fonts.googleapis.com" crossorigin>
<link rel="dns-prefetch preconnect" href="https://fonts.gstatic.com" crossorigin>

<!-- Local Data -->
<link rel="dns-prefetch preconnect" href="https://schema.org" crossorigin>

<!-- SVG -->
<link rel="dns-prefetch preconnect" href="https://www.w3.org" crossorigin>
<?php if ($is_contact == true) 
 { ?>

<!-- Maps -->
<link rel="dns-prefetch preconnect" href="https://maps.googleapis.com" crossorigin>
<link rel="dns-prefetch preconnect" href="https://khms1.googleapis.com" crossorigin>
<?php 
  } // [/link-meta-dns-prefetch] ?>
