<!doctype html>
<html lang="en-US">
<?php // [before-content] ?>
<head>
  <meta charset="utf-8">
  <?php
  // Includes <title>, OG, description <meta> & <link> canonical
  include_once 'seo-meta.php';

  // Prevents native favicon.ico loading in favor of .png version
  // include_once 'favicon.php';

  // Preconnect to required origins
  include_once 'link-meta-dns-prefetch.php';

  // Preload global stylesheets and scripts
  include_once 'link-meta-preload.php';

  // Favicon & global stylesheets
  include_once 'link-meta.php';

  // Google Analytics, etc.
  // include_once 'scripts-head.php';
?>

</head>
<body>

<?php 
  // Nav + Offcanvas menu
  include_once 'top-bar.php'; 
?>
  <!-- Main Layout -->

  <main>

    <?php include_once 'layout.php'; ?>
<?php // [/before-content] ?>