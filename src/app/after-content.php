<!-- After Content -->

</main>

<?php 
include_once 'content-actions.php';       // Actions strip
include_once 'main-footer.php';           // Footer
include_once 'svg-defs.php';              // SVG defs
include_once 'after-content-scripts.php'; // Scripts that need to be loaded after the page content

// https://www.delftstack.com/howto/php/php-html-minifier/
// ob_end_flush();

// [/after-content] 
?>
</body>
</html>