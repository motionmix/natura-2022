<?php // [favicon] ?>
<!-- 
  TODO:
  1. Add app icon/favicon media + site manifest to a PWA partial.
-->
<link rel="icon shortcut" href="data:,"><!-- prevents HTTP request for favicon.ico -->
<link rel="icon" type="image/png" href="<?php echo $imgPATH ?>/favicon.png">

<?php // [/favicon] ?>