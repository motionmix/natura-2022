<!-- Link Meta -->
<!-- Traditional CSS -->

<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Cinzel+Decorative&display=swap" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" crossorigin="anonymous" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor">

<?php if ($is_gallery == true) 
  { ?><!-- Gallery CSS -->

<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/magnific-popup.min.css">
<link rel="stylesheet" href="/assets/css/custom-mfp.css">
<?php 
  } ?>

<!-- INLINE STYLES -->

<style>
/* Bootstrap Overrides */

  .bg-dark {
    --bs-bg-opacity: 1;
    background-color: rgba(137, 118, 22, var(--bs-bg-opacity)) !important;
  }

  .bg-primary {
    --bs-bg-opacity: 1;
    background-color: rgba(80, 122, 7, var(--bs-bg-opacity)) !important;
  }

  .btn-primary {
    --bs-btn-color: #fff;
    --bs-btn-bg: rgb(80,122,7);
  }

  .nav-pills .nav-link.active, 
  .nav-pills .show > .nav-link {
    color: var(--bs-nav-pills-link-active-color);
    background-color: rgb(80,122,7);
  }

  .nav-pills .nav-link {
    color: rgb(80,122,7);
  }

/* Custom CSS */

  /* 1 */
  .bg-extras {
    background-image: url(/assets/img/_hands-1440x480.jpg);
  }

  /* 2 */
  .bg-feet {
    background-image: url(/assets/img/_feet-600x360.jpg);
  }

  /* 3 */
  .bg-hands {
    background-image: url(/assets/img/_hands-600x360.jpg);
  }

  /* 4 */
  .bg-hero {
    background: url(/assets/img/_nail-spa.jpg);
  }

  /* 5 */
  .bg-services {
    background-image: url(/assets/img/_services-1440x480.jpg);
  }

  /* 6 */
  .bg-wax {
    background-image: url(/assets/img/_waxing-600x360.jpg);
  }

  body {
    line-height: 1.625;
  }

  .abs-bottom {
    position: absolute;
    bottom: 0;
    transform: translateX(-50%);
  }

  .ar-4x1 {
    aspect-ratio: 4 / 1;
  }

  .ar-5x1 {
    aspect-ratio: 5 / 1;
  }

  .ar-6x1 {
    aspect-ratio: 6 / 1;
  }

  .bg-white-_75 {
    background-color: rgba(255, 255, 255, 0.75);
  }

  .bg-white-_875 {
    background-color: rgba(255, 255, 255, .875);
  }

  .bg-hero,
  .bg-hands,
  .bg-feet,
  .bg-extras,
  .bg-services,
  .bg-wax {
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
    background-color: transparent;
  }

  .cinzel {
    font-family: 'Cinzel Decorative';
  }

  .formfactor {
    background-size: cover;
    background-position: center center;
    background-repeat: no-repeat;
  }

  .fs-150_ {
    font-size: 150%;
  }

  .icon-offset-tl {
    margin-left: -.375em;
    margin-top: -.125em;
  }

  .icon-offset-t {
    margin-top: -.125em;
  }

  .leading {
    letter-spacing: .0375em;
  }

  .lh-40 {
    line-height: 40px;
  }

  .nav-social a,
  .offcanvas-social a {
    opacity: .875;
  }

  .nav-social a:hover,
  .offcanvas-social a:hover {
    opacity: 1;
  }

  .relative {
    position: relative;
  }

  .semi-opaque {
    opacity: .875;
  }

  .semi-opaque:hover,
  .semi-opaque:focus,
  .semi-opaque:active {
    opacity: 1;
  }

  .service-block-cta {
    display: block;
    transition: transform 400ms;

    width: 84%; 
    height: 19rem;
    border-radius: 1.25rem 1.25rem 0 0;
  }

  .service-block-cta:before {
    display: block;
    position: absolute;
    content: '';
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
    left: 0;
    height: 100%;
    width: 100%;
  }

  .service-block-cta:hover {
    transform: scale(1.0375);
  }

/* hides active service block on individual service pages */

  .extras.other-services .extras-block,
  .feet.other-services .feet-block,
  .hands.other-services .hands-block,
  .wax.other-services .wax-block {
    display: none;
  }

/* forces full opacity and disables pointer on nav-items for current page */

  a.active,
  a.active:hover,
  a.active.semi-opaque,
  .navbar-nav a.active,
  a.active.semi-opaque:hover,
  .navbar-nav a.active:hover {
    opacity: 1 !important;
    cursor: default !important;
    text-decoration: none !important;
  }

</style>
<?php // [/link-meta] ?>