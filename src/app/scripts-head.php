<?php // [scripts-head] ?>

<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $ga_id ?>"></script>
<script>
  window.dataLayer = window.dataLayer || []; 
  
  function gtag(){
    dataLayer.push(arguments);
  }
  
  gtag('js', new Date()); gtag('config', '<?php echo $ga_id ?>');
</script>
<?php // [/scripts-head] ?>
