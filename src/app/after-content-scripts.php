<!-- After content scripts -->

<script>
  /* Dynamic copyright year */
  const d = new Date(); const yr = document.querySelector('#copyrightYear'); yr.innerHTML = d.getFullYear();

  /* Find anchors that match the current path. */
  const activeNavLinks = document.querySelectorAll('a[href="' + location.pathname + '"]');

  /* Stop clicks on active link items */
  for (const activeLinks of activeNavLinks) {
    activeLinks.classList.add('active');

    activeLinks.addEventListener('click', function(event) { 
      event.preventDefault();
    });
  }

</script>

<!-- <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous" async></script> -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous" async></script>

<?php if ($is_gallery == true) { ?>
<!-- Gallery JS -->

<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/magnific-popup@1.1.0/dist/jquery.magnific-popup.min.js" async></script>
<script>
// https://dimsemenov.com/plugins/magnific-popup/documentation.html

$(document).ready(function() {
  
  //  create a groups object
	const groups = {};

  //  create an id for each gallery item
	$('.gallery-item').each(function() {
		let id = parseInt($(this).attr('data-group'), 10);
		if (!groups[id]) { groups[id] = []; }

    //  push gallery item into group
		groups[id].push(this);
	});

	$.each(groups, function() {

    //  apply popup settings to each gallery item
		$(this).magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false,
			gallery: { 
        enabled: true 
      }
		})
	});
});
</script>

<?php } // [/after-content-scripts] ?>