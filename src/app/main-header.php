<!-- Main Header -->

<header class="d-flex flex-wrap justify-content-center py-3">
  <!-- PAGE TITLE -->

  <div class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-dark text-decoration-none">
    <a href="/">
      <img class="d-block mx-auto bi me-2" src="/assets/img/icons/icon-144x144.png" alt="Natura Nail Spa logo mark" width="42" height="42">
    </a>
    
    <span class="fs-4"><?php echo $page_title ?></span>
  </div>

  <!-- SOCIAL NAV -->

  <aside>
    <ul class="nav nav-pills nav-social">
      <li class="nav-item px-1">
        <a class="nav-link link-dark px-2" href="https://facebook.com/<?php echo $site_slug ?>" target="_blank">
          <span class="visually-hidden">Facebook Business Page</span>
          <svg class="bi" width="24" height="24"><use xlink:href="#facebook"></use></svg>
        </a>
      </li>
      <li class="nav-item px-1">
        <a class="nav-link link-dark px-2" href="<?php echo $g_map_url ?>" target="_blank">
          <span class="visually-hidden">Google Business Page</span>
          <svg class="bi" width="24" height="24"><use xlink:href="#google"></use></svg>
        </a>
      </li>
      <li class="nav-item px-1">
        <a class="nav-link link-dark px-2" href="https://instagram.com/<?php echo $ig_handle ?>" target="_blank">
          <span class="visually-hidden">Instagram Profile</span>
          <svg class="bi" width="24" height="24"><use xlink:href="#instagram"></use></svg>
        </a>
      </li>
      <li class="nav-item px-1">
        <a class="nav-link link-dark px-2" href="<?php echo $yelp_url ?>" target="_blank">
          <span class="visually-hidden">Yelp Listing</span>
          <svg class="bi" width="24" height="24"><use xlink:href="#yelp"></use></svg>
        </a>
      </li>
      <li class="nav-item px-1">
        <a class="nav-link link-dark px-2" href="https://twitter.com/<?php echo $site_slug ?>" target="_blank">
          <span class="visually-hidden">Twitter Feed</span>
          <svg class="bi" width="24" height="24"><use xlink:href="#twitter"></use></svg>
        </a>
      </li>
    </ul>
  </aside>
  
</header>
<?php // [/main-header] ?>