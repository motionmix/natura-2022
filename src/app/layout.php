<?php // [layout] ?>
<article>
  <h1 class="visually-hidden"><?php echo $page_title ?></h1>
  <div class="container">

    <!-- HEADER -->

    <?php include_once 'main-header.php'; ?>
  </div>

<?php 
  include_once 'content.php'; ?>

</article>
<?php // [/layout] ?>