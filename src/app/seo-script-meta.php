<!-- SEO Script Meta -->

<script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "NailSalon",
    "name": "<?php echo $site_name ?>",
    "image": "<?php echo $site_url . $imgPATH ?>/logo.png",
    "@id": "",
    "url": "<?php echo $site_url ?>/",
    "telephone": "<?php echo $phone_num ?>",
    "priceRange": "$$",
    "address": {
      "@type": "PostalAddress",
      "streetAddress": "<?php echo $address_st ?>",
      "addressLocality": "<?php echo $address_city ?>",
      "addressRegion": "<?php echo $address_state ?>",
      "postalCode": "<?php echo $address_zip ?>",
      "addressCountry": "<?php echo $address_country ?>"
    },
    "geo": {
      "@type": "GeoCoordinates",
      "latitude": 34.0232819,
      "longitude": -118.4928728
    } ,
    "sameAs": [
      "https://facebook.com/<?php echo $site_slug ?>",
      "<?php echo $g_map_url ?>",
      "https://instagram.com/natura_nail_spa",
      "https://www.yelp.com/biz/natura-nail-spa-santa-monica-2",
      "https://twitter.com/<?php echo $site_slug ?>",
    ]
  }
</script>
<?php // [/seo-script-meta] ?>