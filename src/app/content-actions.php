<div class="container-fluid px-0 mt-5">
  <div class="px-4 py-5 text-center bg-services ar-4x1 d-flex align-items-center">

    <div class="rounded-3 py-5 px-3 d-flex mx-auto container">
      <div class="col-lg-6 mx-auto ">
        <h3 class="cinzel h2 leading">Schedule Appointment</h3>
        <p class="lead">Ready to book an appointment? Give us a call or book online through Yelp!</p>
        <?php include 'actions-strip.php'; ?>

      </div>
    </div>

  </div>
</div>
