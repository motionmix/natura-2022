<!-- SEO Meta OpenGraph -->

<meta name="twitter:card" content="summary">
<meta name="twitter:creator" content="@<?php echo $site_slug ?>">
<!-- 
  NOTE:
  Twitter images require 1:1 aspect ratio.
  <meta name="twitter:image" content="">
  <meta name="twitter:image:alt" content="">
-->
<meta name="twitter:site" content="@<?php echo $site_slug ?>">

<!-- Open Graph -->

<meta property="og:description" content="<?php echo $page_desc ?>">
<!-- 
  NOTE:
  Facebook Open Graph requires images with 1.91:1 aspect ratio (1200x627).
  <meta property=”og:image” content=”<?php echo $imgPATH ?>/og-img=1200x627.jpg”> 
-->
<meta property="og:site_name" content="<?php echo $site_name ?>">
<meta property="og:title" content="<?php echo $page_title ?>">
<meta property="og:type" content="website">
<meta property="og:url" content="<?php echo $site_url ?>/<?php
  echo $page_slug; if ( !($page_slug == '')) 
  { 
    echo '/'; 
  }  
?>">

<?php //[/seo-meta-opengraph] ?>