<?php
// Application paths

$cssPATH = '/assets/css';
$imgPATH = '/assets/img';
$jsPATH = '/assets/js';


// Local data

$address_st = '922 Wilshire Blvd';
$address_city = 'Santa Monica';
$address_state = 'CA';
$address_zip = '90401';
$address_country = 'US';
$phone_num = '13103955652';


// Page booleans

$is_contact = false;
$is_gallery = false;
$is_home = false;


// General Meta

$meta_canonical = '';
$meta_description = '';

// Page vars

$page_desc = '';
$page_slug = '';
$page_title = '';

// Site vars

$site_name = 'Natura Nail Spa';
$site_slug = 'naturanailspa';
$site_url = 'https://naturanailspa.com';


// Google vars

$g_map_url = 'https://www.google.com/maps/place/Natura+Nail+Spa/@34.0163497,-118.5070918,14.42z/data=!4m5!3m4!1s0x80c2a52200665bef:0xbf5857eddc40de01!8m2!3d34.0232763!4d-118.4928488';
$g_verification = '';
$ga_id = 'UA-136327369-1';
$has_g_verification = false;


// Social vars

$ig_handle = 'natura_nail_spa';
$yelp_url = 'https://www.yelp.com/biz/natura-nail-spa-santa-monica-2';


// TODO: Enable minifier for production build

// https://www.delftstack.com/howto/php/php-html-minifier/
// ob_start("minifier");

// function minifier($code) {
//   $search = array(
//     // Remove whitespaces after tags
//     '/\>[^\S ]+/s',
//     // Remove whitespaces before tags
//     '/[^\S ]+\</s',
//     // Remove multiple whitespace sequences
//     '/(\s)+/s',
//     // Removes comments
//     '/<!--(.|\s)*?-->/'
//   );
//   $replace = array('>', '<', '\\1');
//   $code = preg_replace($search, $replace, $code);
//   return $code;
// }

// Minifier ends on after-content.php
?>