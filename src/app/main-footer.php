
<!-- Main Footer -->

<footer class="bg-dark text-white flex-wrap justify-content-between align-items-center py-3 text-center">
  <div class="container px-md-0">

    <div class="row">
      <div class=" col-md-4 col-sm-12 py-3">

        <!-- SITE LINKS -->

        <b class="h4 d-inline-block mb-3 cinzel">Browse Site</b><br>
        <nav>
          <ul class="nav flex-column">
            <li class="nav-item">
              <a href="/about/" class="nav-link px-2 text-white semi-opaque">About</a>
            </li>
            <li class="nav-item">
              <a href="/gallery/" class="nav-link px-2 text-white semi-opaque">Gallery</a>
            </li>
            <li class="nav-item">
              <a href="/services/" class="nav-link px-2 text-white semi-opaque">Services</a>
            </li>
            <li class="nav-item">
              <a href="/contact/" class="nav-link px-2 text-white semi-opaque">Contact</a>
            </li>
          </ul>
        </nav>

      </div>
      <div class=" col-md-4 col-sm-12 py-3">

        <!-- BIZ HOURS -->

        <b class="h4 d-inline-block mb-3 cinzel">Business Hours</b><br>
        <p class="-text-muted">
          <b>Monday - Friday</b><br>
          9:30am - 7:30pm<br>
          <b>Saturday</b><br>
          9:00am - 7:30pm<br>
          <b>Sunday</b><br>
          9:30am - 7:00pm</p>

      </div>
      <div class=" col-md-4 col-sm-12 py-3">

        <!-- ADDRESS -->
        
        <b class="h4 d-inline-block mb-3 cinzel">Natura Nail Spa</b><br>
        <a href="/" class="d-inline-block semi-opaque">
          <img class="mx-auto mb-3" src="/assets/img/n-icon-88x88.png" alt="Natura Nail Spa logo mark" width="42" height="42">
        </a>
        <address class="mb-3">
          922 Santa Monica Blvd.<br>Santa Monica, CA, 90401
        </address>

        <!-- FOOTER SOCIAL -->

        <ul class="list-unstyled d-flex justify-content-center nav-social">
          <li class="px-1">
            <a class="text-white d-inline-block px-2" href="https://facebook.com/naturanailspa" target="_blank" rel="noopener">
              <span class="visually-hidden">Facebook Business Page</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#facebook"></use></svg>
            </a>
          </li>
          <li class="px-1">
            <a class="text-white d-inline-block px-2" href="https://www.google.com/maps/place/Natura+Nail+Spa/@34.0163497,-118.5070918,14.42z/data=!4m5!3m4!1s0x80c2a52200665bef:0xbf5857eddc40de01!8m2!3d34.0232763!4d-118.4928488" target="_blank" rel="noopener">
              <span class="visually-hidden">Google Business Page</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#google"></use></svg>
            </a>
          </li>
          <li class="px-1">
            <a class="text-white d-inline-block px-2" href="https://instagram.com/natura_nail_spa" target="_blank" rel="noopener">
              <span class="visually-hidden">Instagram Profile</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#instagram"></use></svg>
            </a>
          </li>
          <li class="px-1">
            <a class="text-white d-inline-block px-2" href="https://www.yelp.com/biz/natura-nail-spa-santa-monica-2" target="_blank" rel="noopener">
              <span class="visually-hidden">Yelp Listing</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#yelp"></use></svg>
            </a>
          </li>
          <li class="px-1">
            <a class="text-white d-inline-block px-2" href="https://twitter.com/naturanailspa" target="_blank" rel="noopener">
              <span class="visually-hidden">Twitter Feed</span>
              <svg class="bi" width="24" height="24"><use xlink:href="#twitter"></use></svg>
            </a>
          </li>
        </ul>

      </div>
    </div>

    <!-- LEGAL -->

    <p class="mb-0">© 2018-<span id="copyrightYear"></span> <a href="/" class="text-light semi-opaque">Natura Nail Spa</a></p>

  </div>
</footer>

<?php // [/main-footer] ?>
