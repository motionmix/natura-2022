<!-- Content Services -->

<div class="container">
  <div class="d-lg-flex flex-lg-equal my-md-3 row px-0">
    
    <!-- Hands -->

    <aside class="bg-dark pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden col-12 col-sm-12 col-lg-6 relative">
      <div class="my-3 py-3">
        <span class="leading">FOR</span>
        <h2 class="display-5 cinzel mb-0">Hands</h2>
        <p class="lead">Magnificent manicures and more!</p>
      </div>
      <a href="/services/hands/" class="d-block bg-hands shadow mx-auto formfactor semi-opaque service-block-cta relative"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>

    <!-- Feet -->

    <aside class="bg-light pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden col-12 col-sm-12 col-lg-6 relative">
      <div class="my-3 p-3">
        <span class="leading">FOR</span>
        <h2 class="display-5 cinzel mb-0">Feet</h2>
        <p class="lead">Pampered pedicures and more!</p>
      </div>
      <a href="/services/feet/" class="d-block bg-feet formfactor shadow mx-auto semi-opaque service-block-cta"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>

  </div>
  <div class="d-lg-flex flex-lg-equal mt-md-3 mb-md-5 row">

    <!-- Wax -->

    <aside class="bg-light pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden col-12 col-sm-12 col-lg-6 relative">
      <div class="my-3 py-3">
        <span class="leading">FOR</span>
        <h2 class="display-5 cinzel mb-0">Waxing</h2>
        <p class="lead">Wonderful waxing services!</p>
      </div>
      <a href="/services/wax/" class="bg-wax formfactor shadow mx-auto d-block semi-opaque service-block-cta"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>

    <!-- Extras -->

    <aside class="bg-primary pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden col-12 col-sm-12 col-lg-6 relative">
      <div class="my-3 p-3">
        <span class="leading">FOR</span>
        <h2 class="display-5 cinzel mb-0">Add-ons</h2>
        <p class="lead">Glossy gels and more!</p>
      </div>
      <a href="/services/add-ons/" class="d-block bg-extras formfactor shadow mx-auto semi-opaque service-block-cta"><span class="btn btn-dark d-inline-block my-5 abs-bottom">View Menu</span></a>
    </aside>

  </div>
</div>
<?php // [/content-services] ?>