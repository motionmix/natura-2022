<div class="container mt-1">
	<div class="row">
		<div class="col-md-4 col-lg-3">

			<!-- STREET ADDRESS -->
			
			<h3 class="cinzel mb-3 h4">
				Street Address
			</h3>
			<address>
				<span class="address">
					922 Wilshire Blvd <br />
					Santa Monica, CA 90401
				</span>
			</address>
			<hr class="my-4">

			<!-- BIZ HOURS -->

			<h3 class="cinzel mb-3 h4">
				Business Hours
			</h3>
			<ul class="list-group">
				<li class="list-group-item">
					<b>Mon:</b>
					<span>9:30 am - 7:30 pm</span>
				</li>
				<li class="list-group-item">
					<b>Tue:</b> 
					<span>9:30 am - 7:30 pm</span>
				</li>
				<li class="list-group-item">
					<b>Wed:</b> 
					<span>9:30 am - 7:30 pm</span>
				</li>
				<li class="list-group-item">
					<b>Thu:</b> 
					<span>9:30 am - 7:30 pm</span>
				</li>
				<li class="list-group-item">
					<b>Fri:</b> 
					<span>9:30 am - 7:30 pm</span>
				</li>
				<li class="list-group-item">
					<b>Sat:</b> 
					<span>9:00 am - 7:30 pm</span>
				</li>
				<li class="list-group-item">
					<b>Sun:</b> 
					<span>9:30 am - 7:00 pm</span>
				</li>
			</ul>
		</div>

		<div class="col-md-8 col-lg-9">

			<!-- MAP -->

			<h3 class="cinzel mb-3 h4">Location Map</h3>

			<div id="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3306.802478657267!2d-118.49503748496006!3d34.023280726703426!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2a52200665bef%3A0xbf5857eddc40de01!2sNatura+Nail+Spa!5e0!3m2!1sen!2sus!4v1548826705071"
					title="Map to Natura Nail Spa"
					allowfullscreen 
					width="800" 
					height="450"
					class="w-100">
				</iframe>
			</div>
		</div>
	</div>
	
</div>
