<?php
	// Global Vars
	include_once './../app/vars.php';

	$is_contact = true;

	// Local vars
	$page_desc = 'Get in touch to book an appointment, visit us on social, or find our location on Google Maps!';
	$page_slug = 'contact';
	$page_title = 'Contact';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../app/before-content.php';

	// Inner HTML of <main>.
	// Includes: document meta, heading, content, footing, aside.
	include_once 'content.php';

	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../app/after-content.php';
?>