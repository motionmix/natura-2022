<div class="container-fluid px-0">

  <!-- HERO -->
  
  <div class="px-4 py-5 mb-5 text-center bg-hero">
    <div class="rounded-3 py-5 px-3 d-flex flex-column mx-auto container bg-white-_875">
      <h1 class="-display-4 fw-normal mb-0">
        <small>Welcome To <br></small>
        <span class="cinzel display-4 fw-bold">Natura Nail Spa</span>
      </h1>
      <h2 class="fw-normal">A Santa Monica Nail Salon</h2>
      <div class="col-lg-6 mx-auto">
        <p class="lead mb-4">All your nailcare needs and variety of nail and beauty services are here at Natura &mdash; everything from Mani Pedis to eyebrow waxing, and more!</p>
        
        <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
          <a href="/about/" class="btn btn-primary btn-lg px-4 gap-3">More About Natura</a>
          <a href="/gallery/" class="btn btn-secondary btn-lg px-4">Explore Gallery</a>
        </div>
      </div>
    </div>
  </div>

</div>

<div class="container px-0">

  <!-- SERVICES JUMBOTRON -->

  <div class="p-5 mb-4 bg-light rounded-3">
    <section class="container-fluid py-5 text-center">
      <h2 class="display-5 fw-bold cinzel">Our Services</h2>
      <p class="fs-4">Schedule an appointment &mdash; sit back and relax in one of our comfy personal sofas!<br>Our professional staff will pamper you from head to toe, giving you a fresh, revitalizing experience.</p>
      <a href="/services/" class="btn btn-secondary btn-lg">Explore Services</a>
    </section>
  </div>

</div>

<?php include_once './app/content-services.php' ?>