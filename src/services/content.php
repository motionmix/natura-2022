<div class="container-fluid px-0 mb-5">

  <!-- HERO -->
  
  <div class="px-4 py-5 text-center bg-services ar-4x1 d-flex align-items-center">
    <div class="rounded-3 py-5 px-3 d-flex flex-column mx-auto container">

      <h2 class="cinzel leading">Manicures, Pedicures, Waxing and more!</h2>
      <p class="lead">Natura Nail Spa has the nail salon services you need to feel like a refreshed, revitalized new you!</p>

      <div class="col-lg-6 mx-auto ">
        <div class="d-grid gap-2 d-sm-flex justify-content-sm-center">
          <a href="/gallery/" class="btn btn-primary btn-lg px-4 gap-3">Explore Gallery</a>
          <a href="<?php echo $yelp_url ?>" target="_blank" rel="noopener" class="btn btn-secondary btn-lg px-4">
            <svg width="20" height="20" class="icon-offset-tl"><use xlink:href="#yelp"/></svg>&nbsp;
            Book Appointment
          </a>
        </div>
      </div>
      
    </div>
  </div>

</div>

<?php include_once './../app/content-services.php' ?>