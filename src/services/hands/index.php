<?php
	// Global Vars
	include_once './../../app/vars.php';

	// Local vars
	$page_desc = 'Check out our magnificent menu of manicure services, acrylics, dipping powder, Gel-X and more!';
	$page_slug = 'hands';
	$page_title = 'Salon Services &mdash; For Hands';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../../app/before-content.php';

	// 
	include_once './../../app/other-services.php';

	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../../app/after-content.php';
?>
