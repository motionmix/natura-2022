<div class="container-fluid px-0 mb-5">
  <div class="px-4 py-5 text-center bg-hands ar-4x1 d-flex align-items-center mb-4">

    <!-- HERO -->
  
    <div class="rounded-3 py-5 px-3 d-flex mx-auto container bg-white-_75">
      <div class="mx-auto">
        <p>
          <a href="/">
            <img class="d-block mx-auto mb-4" src="/assets/img/icons/icon-144x144.png" alt="Natura logo mark" width="72" height="72">
          </a>
        </p>
        <p class="fs-150_ mb-0 cinzel">Our marvelous menu of manicure services</p>
        <p class="lead mb-4">Everything from acrylics to dipping powder to Gel-X and more!</p>
        <?php include './../../app/actions-strip.php'; ?>

      </div>
    </div>

  </div>
</div>

<section class="container">
	<div class="row">

    <div class="col col-md-10 offset-md-1 col-lg-8 offset-lg-2">
      <h2 class="cinzel leading">Manicures</h2>
      <ul class="list-group lead">
        <li class="list-group-item"><strong>Classic Natura Mani</strong> &mdash; Softening soak. File, shape, cuticle trim and care, 5-min. hand massage, cuticle oil, and polish.</li>
        <li class="list-group-item"><strong>Natura Gelicure Mani</strong> &mdash; File, shape, cuticle trim and care. Includes 5-min. hand massage, cuticle oil, and gel polish.</li>
        <li class="list-group-item"><strong>Rockstar Gel Mani</strong> &mdash; Polish or speacial gel removal. File, shape, cuticle trim and care. Includes 5-min. hand massage, cuticle oil, and glitter gel polish.</li>
        <li class="list-group-item"><strong>Gel Removal Mani</strong> &mdash; Special gel removal. File, shape, cuticle trim and care. Includes 5-min. hand massage, cuticle oil, and polish.</li>
        <li class="list-group-item"><strong>Mini-Mani</strong> &mdash; Trim, file, shape, and polish</li>
        <li class="list-group-item"><strong>Kid's Mani Please</strong> &mdash; Trim, file, shape, and polish</li>
        <li class="list-group-item"><strong>Soft as Silk Mani</strong> &mdash; Softening soak. File, shape, cuticle trim and care with buttercream oil. Hot towel wrap and beeswax. Includes 5-min. hand massage, and polish.</li>
        <li class="list-group-item"><strong>Exfoliating Green Tea and Lemongrass Scrub Mani</strong> &mdash; Classic Natura Mani with organic scrub and a hot towel wrap. Includes 5-min. handd massage.</li>
        <li class="list-group-item"><strong>Detox All-Natural Mani</strong> &mdash; Softening soak. File, shape, cuticle trim and care with organic cuticle softening oil. Organic lotion. Includes 5-min. hand massage. Organic base, top, and polish.</li>
        <li class="list-group-item"><strong>Nourishing Organic Green Tea and Lemongrass Scrub Mani</strong> &mdash; File, shape, cuticle trim and care with buttercream oil and polish. Includes green tea and lemongrass scrub, 5-min. hand massage and a hot towel wrap.</li>
        <li class="list-group-item"><strong>Invigorating Mint and Lavender Organic Sugar Scrub Mani</strong> &mdash; File, shape, cuticle trim and care with buttercream oil and polish. Includes organic mint and lavender sugar scrub, 5-min hand massage and a hot towel wrap.</li>
        <li class="list-group-item"><strong>Citrus Paradise Scrub Mani</strong> &mdash; CLassic Natura Mani with mango pr pink grapefruit sugar scrub. Includes  5-min. hand massage and a hot towel wrap.</li>
        <li class="list-group-item"><strong>Make My Hands Smooth Mani</strong> &mdash; Classic Natura Mani with sea salt scrub. Hot towel wrap and beeswax.</li>
        <li class="list-group-item"><strong>Happy Hands Mani</strong> &mdash; Softening soak. File, shape, cuticle trim, and care. Sea salt scrub, hot towel wrap, and beeswax. Includes 10-min. hand massage, cuticle oil, and polish.</li>
        <li class="list-group-item"><strong>Natura's Royal Mani</strong> &mdash; Softening soak. File, shape, cuticle trim, and care. Sea salt scrub followed by hot towel wrap. Cooling gel or mint mask followed by hot towel wrap, and beeswax. Includes 10-min. hand massage, cuticle oil, and polish.</li>
      </ul>

      <div class="row my-5">
        <?php include './../../app/actions-strip.php'; ?>

      </div>

      <h3 class="h2 services-heading cinzel leading my-5">Acrylics, Dipping Powder, <wbr><small>&amp;</small> Gel-X</h3>

      <ul class="list-group lead">
        <li class="list-group-item">Acrylic Removal</li>
        <li class="list-group-item">Regular Acrylic Fill</li>
        <li class="list-group-item">Pink and White Acrylic Fill</li>
        <li class="list-group-item">Regular Acrylic Full Set</li>
        <li class="list-group-item">Pink and White Acrylic Full Set</li>
        <li class="list-group-item">Nail Fix</li>
        <li class="list-group-item">Cut Down</li>
        <li class="list-group-item">Extra Long Acrylic Tips</li>
        <li class="list-group-item">Dipping Powder</li>
        <li class="list-group-item">Dipping Powder Removal</li>
        <li class="list-group-item">Gel-X Full Set</li>
        <li class="list-group-item">Gel-X Full Set with Removal</li>
      </ul>
    </div>

		<div class="row my-5">
      <?php include './../../app/actions-strip.php'; ?>

		</div>

	</div>	
</section>