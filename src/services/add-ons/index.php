<?php
	// Global Vars
	include_once './../../app/vars.php';

	// Local vars
	
	$page_desc = 'Check out our menu of awesome add-ons from French Polish to Rockstar Gelicure to Paraffin Treatments and more!';
	$page_slug = 'extras';
	$page_title = 'Salon Services &mdash; Add-ons &amp; Extras';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../../app/before-content.php';
	include_once './../../app/other-services.php';


	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../../app/after-content.php';
?>
