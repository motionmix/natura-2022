<div class="container-fluid px-0 mb-5">
  <div class="px-4 py-5 text-center bg-extras ar-4x1 d-flex align-items-center mb-4">

    <!-- HERO -->
  
    <div class="rounded-3 py-5 px-3 d-flex mx-auto container bg-white-_75">
      <div class="mx-auto col-lg-8">
        <p>
          <a href="/">
            <img class="d-block mx-auto mb-4" src="/assets/img/icons/icon-144x144.png" alt="Natura logo mark" width="72" height="72">
          </a>
        </p>
        <p class="fs-150_ mb-0 cinzel">Our marvelous menu of add-ons</p>
        <p class="lead mb-4">From French Polish to Rockstar Gelicure to Paraffin Treatments and more!</p>
        <?php include './../../app/actions-strip.php'; ?>

      </div>
    </div>
    
  </div>
</div>

<section class="container">
	<div class="row">

    <div class="col col-md-10 offset-md-1 col-lg-8 offset-lg-2">
      <h2 class="cinzel leading">Add-ons <small>&amp;</small> Extras</h2>
      <ul class="list-group lead">
        <li class="list-group-item"><strong>Massage</strong></li>
        <li class="list-group-item"><strong>French Polish</strong></li>
        <li class="list-group-item"><strong>Polish Change</strong></li>
        <li class="list-group-item"><strong>Nail Art</strong></li>
        <li class="list-group-item"><strong>Acrylic Nail Fix</strong></li>
        <li class="list-group-item"><strong>Gel Removal</strong></li>
        <li class="list-group-item"><strong>Gel Polish Change</strong></li>
        <li class="list-group-item"><strong>Rockstar Gelicure per Nail</strong></li>
        <li class="list-group-item"><strong>Refresh Mint Clay Mask</strong></li>
        <li class="list-group-item"><strong>Paraffin Treatment</strong></li>
        <li class="list-group-item"><strong>Organic Moisturizing Scrub</strong></li>
        <li class="list-group-item"><strong>Callus Repair Treatment</strong></li>
        <li class="list-group-item"><strong>Gel Add-On</strong> <small><i>(Add gel to any mani/pedi)</i></small></li>
      </ul>

      <div class="row my-5">
        <?php include './../../app/actions-strip.php'; ?>

      </div>
    </div>

	</div>	
</section>