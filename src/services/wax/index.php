<?php
	// Global Vars
	include_once './../../app/vars.php';

	// Local vars
	$page_desc = 'Check out our wonderful waxing services for eyebrows, underarms, biniki-line and more!';
	$page_slug = 'wax';
	$page_title = 'Salon Services &mdash; Wax';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../../app/before-content.php';

	// Inner HTML of <main>.
	// Includes: document meta, heading, content, footing, aside.
	// include_once 'content.php';

	// 
	include_once './../../app/other-services.php';

	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../../app/after-content.php';
?>
