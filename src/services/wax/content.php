<div class="container-fluid px-0 mb-5">  
  <div class="px-4 py-5 text-center bg-wax ar-4x1 d-flex align-items-center mb-4">

    <!-- HERO -->

    <div class="rounded-3 py-5 px-3 d-flex mx-auto container bg-white-_75">
      <div class="mx-auto col-lg-10">
        <p>
          <a href="/">
            <img class="d-block mx-auto mb-4" src="/assets/img/icons/icon-144x144.png" alt="Natura logo mark" width="72" height="72">
          </a>
        </p>
        <p class="fs-150_ mb-0 cinzel">Our wonderful menu of waxing services</p>
        <p class="lead mb-4">Everything from eyebrows to underarms to biniki-line and more!</p>
        <?php include './../../app/actions-strip.php'; ?>

      </div>
    </div>
    
  </div>
</div>

<section class="container">
	<div class="row">
    <div class="col col-md-10 offset-md-1 col-lg-8 offset-lg-2">
      <ul class="list-group lead">
        <li class="list-group-item">Chin, Upper Lip, or Stomach</li>
        <li class="list-group-item">Cheeks or Underarms</li>
        <li class="list-group-item">Eyebrows</li>
        <li class="list-group-item">Face <i>(Excludes Eyebrows)</i></li>
        <li class="list-group-item">Half Arm / Full Arm</li>
        <li class="list-group-item">Half Leg / Full Leg</li>
        <li class="list-group-item">Just Side Bikini</li>
        <li class="list-group-item">Half Off Bikini / All Off Bikini</li>
        <li class="list-group-item">Spot Treatment</li>
        <li class="list-group-item">Back Wax</li>
      </ul>
    </div>

		<div class="row">
      <?php include './../../app/actions-strip.php'; ?>

    </div>
	</div>	
</section>