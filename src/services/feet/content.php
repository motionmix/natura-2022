<div class="container-fluid px-0 mb-5">
  <div class="px-4 py-5 text-center bg-feet ar-4x1 d-flex align-items-center mb-4">

    <!-- HERO -->
  
    <div class="rounded-3 py-5 px-3 d-flex mx-auto container bg-white-_75">
      <div class="mx-auto">
        <p>
          <a href="/">
            <img class="d-block mx-auto mb-4" src="/assets/img/icons/icon-144x144.png" alt="Natura logo mark" width="72" height="72">
          </a>
        </p>
        <p class="fs-150_ mb-0 cinzel">Our marvelous menu of pedicure services</p>
        <p class="lead mb-4">Everything from classic pedi to Gelicure pedi to the full Royal treatment and more!</p>
        <?php include './../../app/actions-strip.php'; ?>

      </div>
    </div>
    
  </div>
</div>

<section class="container">
	<div class="row">

    <div class="col col-md-10 offset-md-1 col-lg-8 offset-lg-2">
      <h2 class="cinzel leading">Pedicures</h2>
      <ul class="list-group lead">
        <li class="list-group-item"><strong>Classic Natura Pedi</strong> &mdash; Softening soak. File, shape, cuticle trim/care, 5-min. foot massage, cuticle oil, and polish.</li>
        <li class="list-group-item"><strong>Natura Gelicure Pedi</strong> &mdash; Polish or special gel removal. File, shape, cuticle trim and care. Includes 5-min. foot massage, cuticle oil, and gel polish.</li>
        <li class="list-group-item"><strong>Rockstar Gel Pedi</strong> &mdash; Polish or special gel removal. File, shape, cuticle trim and care. Includes 5-min. foot massage, cuticle oil, and glitter gel polish.</li>
        <li class="list-group-item"><strong>Gel Removal Pedi</strong> &mdash; Special gel removal. File, shape, cuticle trim and care. Includes 5-min. foot massage, cuticle oil, and classic polish.</li>
        <li class="list-group-item"><strong>Mini-Pedi</strong> &mdash; Trim, file, shape, and polish</li>
        <li class="list-group-item"><strong>Kid's Pedi Please</strong> &mdash; Trim, file, shape, and polish</li>
        <li class="list-group-item"><strong>Soft as Silk Pedi</strong> &mdash; Softening soak. File, shape, cuticle trim and care with buttercream oil. Hot towel wrap and beeswax. Includes 5-min. foot massage, and polish.</li>
        <li class="list-group-item"><strong>Exfoliating Green Tea and Lemongrass Sea Salt Scrub Pedi</strong> &mdash; Classic Natura Pedi with sea salt scrub and a hot towel wrap. Includes 5.-min foot massage.</li>
        <li class="list-group-item"><strong>Detox All-Natural Pedi</strong> &mdash; Softening soak. File, shape, cuticle trim and care with organic cuticle softening oil. Organic lotion. Includes 5-min. foot massage. Organic base, top, and polish.</li>
        <li class="list-group-item"><strong>Nourishing Organic Seaweed Scrub Pedi</strong> &mdash; Classic Natura Pedi with seaweed scrub and a hot towel wrap.</li>
        <li class="list-group-item"><strong>Invigorating Mint and Lavendar Organic Sugar Scrub Pedi</strong> &mdash; Classic Natura Pedi with organic sugar scrub and a hot towel wrap. Includes polish and 5-min. foot massage.</li>
        <li class="list-group-item"><strong>Citris Paradise Scrub Pedi</strong> &mdash; Classic Natura Pedi with mango or pink grapdefruit sugar scrub. Includes 5-min. foot massage and hot towel wrap.</li>
        <li class="list-group-item"><strong>Make My Feet Smooth Pedi</strong> &mdash; Classic Natura Pedi with sea salt scrub. Hot towel wrap and beeswax.</li>
        <li class="list-group-item"><strong>Happy Feet Pedi</strong> &mdash; Softening soak. File, shape, cuticle trim, and care. Sea salt scrub, hot towel wrap, and beeswax. Includes 10-min. foot massage, cuticle oil, and polish.</li>
        <li class="list-group-item"><strong>Natura's Royal Pedi</strong> &mdash; Softening soak. File, shape, cuticle trim, and care. Sea salt scrub followed by hot towel wrap. Cooling gel or mint mask followed by hot towel wrap, and beeswax. Includes 10-min. foot massage, cuticle oil, and polish.</li>
        <li class="list-group-item"><strong>Callus Repair Pedi</strong> &mdash; Softening soak. File, shape, cuticle trim and care with callus treatment. Clean with hot towel. Includes 5-min. foot massage and polish.</li>
      </ul>

      <div class="row my-5">
        <?php include './../../app/actions-strip.php'; ?>

      </div>
    </div>

	</div>	
</section>