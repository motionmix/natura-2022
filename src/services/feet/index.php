<?php
	// Global Vars
	include_once './../../app/vars.php';

	// Local vars
	$page_desc = 'Check out our polished menu of pampered pedicure services!';
	$page_slug = 'feet';
	$page_title = 'Salon Services &mdash; For Feet';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../../app/before-content.php';

	// 
	include_once './../../app/other-services.php';
	
	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../../app/after-content.php';
?>
