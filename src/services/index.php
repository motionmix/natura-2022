<?php
	// Global Vars
	include_once './../app/vars.php';

	// Local vars
	$page_desc = 'Manicures, Pedicures, Waxing and more! Start here for a menu of all our nail salon services.';
	$page_slug = 'services';
	$page_title = 'Salon Services';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../app/before-content.php';

	// Inner HTML of <main>.
	// Includes: document meta, heading, content, footing, aside.
	include_once 'content.php';

	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../app/after-content.php';
?>
