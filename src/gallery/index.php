<?php
	// Global Vars
	include_once './../app/vars.php';

	// Local vars
	$is_gallery = true;

	$page_desc = 'Checkout our gallery of gorgeous nail art and nail services!';
	$page_slug = 'gallery';
	$page_title = 'Services Gallery';

	// Everything above <main>.
	// Includes: head, global meta, nav & styles.
	include_once './../app/before-content.php';

	// Inner HTML of <main>.
	// Includes: document meta, heading, content, footing, aside.
	include_once 'content.php';

	// Everything below </main>
	// Includes: App footer, scripts, analytics, etc.
	include_once './../app/after-content.php';
?>