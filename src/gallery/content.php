<!-- GALLERY ITEMS -->

<div class="container mb-5">
  <div class="row">

    <div class="col-12 col-sm-4 my-2">
      <a href="/assets/img/gallery/1-light-purple-chrome.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Light purple chrome nails.">
        <div class="gallery-box">
          <div class="gallery-image img-1"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/2-ombre-dip-powder-acrylic-nails.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Ombre dip powder acrylic nails.">
        <div class="gallery-box">
          <div class="gallery-image img-2"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/3-halloween-nails.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Halloween nails!!!">
        <div class="gallery-box">
          <div class="gallery-image img-3"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/4-face-nail-art.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Face nail art.">
        <div class="gallery-box">
          <div class="gallery-image img-4"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/5-cute-gel-nail-design.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Cute gel nail design.">
        <div class="gallery-box">
          <div class="gallery-image img-5"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/6-gel-ombre-nails.jpg"  class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Gel ombre nails!">
        <div class="gallery-box">
          <div class="gallery-image img-6"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/7-acrilycs-pastel-design.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Acrilycs pastel design.">
        <div class="gallery-box">
          <div class="gallery-image img-7"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/8-mexican-fiesta-nails.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Mexican fiesta nails!">
        <div class="gallery-box">
          <div class="gallery-image img-8"></div>
        </div>
      </a>
    </div>

    <div class="col-6 col-sm-4 my-2">
      <a href="/assets/img/gallery/9-groovy-nails.jpg" class="gallery-item gallery-item-popup thumbnail" 
        data-group="gallery-1" title="Groovy nails.">
        <div class="gallery-box">
          <div class="gallery-image img-9"></div>
        </div>
      </a>
    </div>

  </div>
</div>
